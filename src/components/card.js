import React from "react"

import "./card.css"

const Card = props => {
  console.log(props.monopattini)
  const cardGrid = props.monopattini.map(monopattino => {
    const {
      id,
      name,
      image,
      peso,
      autonomia,
      autonomiakl,
      velocita,
      carico,
      batteria,
      caricaBatteria,
      caricaBatteriam,
      prezzo,
      dimensioni,
      dimensioniRipiegato,
      extra,
    } = monopattino

    return (
      <div className="card" key={id}>
        <h2>{name}</h2>
        <img src={image} alt={name} />
        <p>
          <strong>Peso: </strong>
          {`${peso} Kg`}
        </p>
        <p>
          <strong>
            {autonomia ? "Autonomia in tempo: " : "Autonomia in kilometri: "}
          </strong>
          {autonomia ? autonomia : autonomiakl}
        </p>
        <p>
          <strong>Velocità: </strong>
          {`${velocita} kl/h`}
        </p>
        <p>
          <strong>Batteria: </strong>
          {`${batteria} W`}
        </p>
        <p>
          <strong>
            {caricaBatteria
              ? "Carica della batteria ore: "
              : "Carica della batteria min: "}
          </strong>
          {caricaBatteria ? caricaBatteria : caricaBatteriam}
        </p>
        <p>
          <strong>Prezzo: </strong>
          {prezzo ? `Da ${prezzo[0]} a ${prezzo[1]} Euro` : null}
        </p>
        <p>
          <strong>Dimensioni: </strong>
          {dimensioni}
        </p>
        <p>
          <strong>
            {dimensioniRipiegato ? "Dimensioni da ripiegato:" : null}{" "}
          </strong>
          {dimensioniRipiegato ? dimensioniRipiegato : null}
        </p>
        <p>
          <strong>Carico: </strong>
          {`${carico} kl`}
        </p>
        <p>
          <strong>Extra: </strong>
          {extra}
        </p>
      </div>
    )
  })

  return cardGrid
}

export default Card
