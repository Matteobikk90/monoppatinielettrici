import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Card from "./card"
import "./layout.css"

const Layout = () => {
  const data = useStaticQuery(graphql`
    {
      allDataJson {
        nodes {
          monopattini {
            autonomia
            autonomiakl
            batteria
            carico
            dimensioniRipiegato
            dimensioni
            extra
            id
            name
            image
            peso
            velocita
            prezzo
            caricaBatteria
            caricaBatteriam
          }
        }
      }
    }
  `)

  const { monopattini } = data.allDataJson.nodes[0]

  return (
    <>
      <main>
        <Card monopattini={monopattini} />
      </main>
    </>
  )
}

export default Layout
